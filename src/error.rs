use std::error::Error as StdError;
use std::fmt;

#[derive(Clone, Debug)]
pub struct Error(pub String);

impl fmt::Display for Error {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.0)
	}
}

impl StdError for Error {}
