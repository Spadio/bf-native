use clap::{AppSettings, Clap, ValueHint};
use inkwell::context::Context;
use inkwell::passes::PassManager;
use inkwell::targets::{
	CodeModel, InitializationConfig, RelocMode, Target, TargetMachine, TargetTriple,
};
use inkwell::OptimizationLevel;
use log::LevelFilter;
use simplelog::{ColorChoice, Config, TermLogger, TerminalMode};
use std::path::PathBuf;
use std::str::FromStr;

mod ast;
mod debug_info;
mod error;
mod functions;
mod program;
use program::Program;

#[derive(Clone, Copy, Debug)]
enum OutputFormat {
	LlvmIr,
	Object,
	Binary,
}

impl FromStr for OutputFormat {
	type Err = &'static str;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		match s.to_lowercase().as_str() {
			"obj" => Ok(Self::Object),
			"ir" | "ll" => Ok(Self::LlvmIr),
			"bin" | "exe" => Ok(Self::Binary),
			_ => Err("invalid output format"),
		}
	}
}

#[derive(Clone, Debug)]
struct ArgsTarget(String);

impl ArgsTarget {
	pub fn to_triple(&self) -> TargetTriple {
		if self.0.is_empty() {
			TargetMachine::get_default_triple()
		} else {
			TargetMachine::normalize_triple(&TargetTriple::create(&self.0))
		}
	}
}

impl From<&str> for ArgsTarget {
	#[inline]
	fn from(value: &str) -> Self {
		Self(value.to_string())
	}
}

/// LLVM frontend for the brainf*ck language.
#[derive(Clap)]
#[clap(version = env!("CARGO_PKG_VERSION"), author = clap::crate_authors!())]
#[clap(setting = AppSettings::ColoredHelp)]
pub struct Args {
	/// Enable verbose output.
	#[clap(short = 'v', long)]
	verbose: bool,

	/// Number of cells to reserve.
	#[clap(short = 'c', long, default_value = "32768")]
	cell_count: u32,

	/// Maximum number of iterations per loop that can occur while performing the const cell pass.
	#[clap(long, default_value = "1000000")]
	const_iters: usize,

	/// The format to generate for the output.
	#[clap(short, long, parse(try_from_str), default_value = "exe", possible_values = &["obj", "ir", "ll", "bin", "exe"])]
	emit: OutputFormat,

	/// Output file.
	#[clap(short, long, parse(from_os_str), value_hint = ValueHint::FilePath)]
	output: Option<PathBuf>,

	/// LLVM target triple to build for.
	#[clap(long, parse(from_str), default_value = "")]
	target: ArgsTarget,

	/// Input file.
	#[clap(index(1), value_hint = ValueHint::FilePath)]
	input: PathBuf,
}

fn main() {
	let args = Args::parse();
	let target = args.target.clone();

	let level_filter = if args.verbose {
		LevelFilter::Trace
	} else {
		LevelFilter::Warn
	};
	TermLogger::init(
		level_filter,
		Config::default(),
		TerminalMode::Mixed,
		ColorChoice::Auto,
	)
	.unwrap();

	Target::initialize_all(&InitializationConfig::default());

	let ctx = Context::create();
	let builder = ctx.create_builder();
	let pass_manager = {
		let pass_manager = PassManager::create(());
		pass_manager.add_global_dce_pass();
		pass_manager.add_reassociate_pass();
		pass_manager.add_function_inlining_pass();
		pass_manager.add_ipsccp_pass();
		pass_manager.add_dead_arg_elimination_pass();
		pass_manager.add_instruction_simplify_pass();
		pass_manager.add_instruction_combining_pass();
		pass_manager.add_promote_memory_to_register_pass();
		pass_manager.add_gvn_pass();
		pass_manager.add_ind_var_simplify_pass();
		pass_manager.add_loop_unroll_pass();
		pass_manager.add_cfg_simplification_pass();
		pass_manager.add_dead_store_elimination_pass();
		pass_manager
	};

	let target = args.target.clone();

	let mut program = Program::new(&ctx, &builder, args);
	if let Err(e) = program.generate() {
		log::error!("Failed to generate program for the given source:\n{}", e);
		std::process::exit(1);
	}

	let target_machine = {
		let target_triple = target.to_triple();
		let target = Target::from_triple(&target_triple).unwrap();
		target
			.create_target_machine(
				&target_triple,
				&TargetMachine::get_host_cpu_name().to_string(),
				&TargetMachine::get_host_cpu_features().to_string(),
				OptimizationLevel::Aggressive,
				RelocMode::Default,
				CodeModel::Default,
			)
			.unwrap()
	};
	program
		.emit(&pass_manager, target_machine)
		.expect("failed to emit program");
}
