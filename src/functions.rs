use inkwell::builder::Builder;
use inkwell::context::Context;
use inkwell::module::{Linkage, Module};
use inkwell::types::BasicType;
use inkwell::values::{BasicValue, FunctionValue};
use inkwell::AddressSpace;
use std::rc::Rc;

macro_rules! __impl_functions_inner {
	( @ty $self:ident * $base:ident ) => { __impl_functions_inner!(@ty $self $base).ptr_type(AddressSpace::Generic) };
	( @ty $self:ident [$base:ident; $len:expr] ) => { __impl_functions_inner!(@ty $self $base).array_type($len) };
	// LLVM IR types don't care about the signedness of integers.
	( @ty $self:ident u8 ) => { $self.ctx.i8_type() };
	( @ty $self:ident u16 ) => { $self.ctx.i16_type() };
	( @ty $self:ident u32 ) => { $self.ctx.i32_type() };
	( @ty $self:ident u64 ) => { $self.ctx.i64_type() };
	( @ty $self:ident u128 ) => { $self.ctx.i128_type() };
	// Default type handling.
	( @ty $self:ident $base:ident ) => { paste::paste! { $self.ctx.[< $base _type >]() } };

	( @has_varargs ... $($tt:tt)* ) => { true };
	( @has_varargs * $_:ident $( , $($tt:tt)* )? ) => { __impl_functions_inner!(@has_varargs $( $($tt)* )?); };
	( @has_varargs [$_:ident; len] $( , $($tt:tt)* )? ) => { __impl_functions_inner!(@has_varargs $( $($tt)* )?); };
	( @has_varargs $_:ident $( , $($tt:tt)* )? ) => { __impl_functions_inner!(@has_varargs $( $($tt)* )?); };
	( @has_varargs ) => { false };

	( @args $self:ident [ $( $stack:expr ),* ] * $base_ty:ident , $($tt:tt)* ) => { __impl_functions_inner!(@args $self [$($stack,)* __impl_functions_inner!(@ty $self * $base_ty).as_basic_type_enum()] $($tt)*) };
	( @args $self:ident [ $( $stack:expr ),* ] * $base_ty:ident ) => { &[ $($stack,)* __impl_functions_inner!(@ty $self * $base_ty).as_basic_type_enum() ] };
	( @args $self:ident [ $( $stack:expr ),* ] [$base_ty:ident; $len:expr] , $($tt:tt)* ) => { __impl_functions_inner!(@args $self [$($stack,)* __impl_functions_inner!(@ty $self [$base_ty; $len]).as_basic_type_enum()] $($tt)*) };
	( @args $self:ident [ $( $stack:expr ),* ] [$base_ty:ident; $len:expr] ) => { &[ $($stack,)* __impl_functions_inner!(@ty $self [$base_ty; $len]).as_basic_type_enum() ] };
	( @args $self:ident [ $( $stack:expr ),* ] $base_ty:ident , $($tt:tt)* ) => { __impl_functions_inner!(@args $self [$($stack,)* __impl_functions_inner!(@ty $self $base_ty).as_basic_type_enum()] $($tt)*) };
	( @args $self:ident [ $( $stack:expr ),* ] $base_ty:ident ) => { &[ $($stack,)* __impl_functions_inner!(@ty $self $base_ty).as_basic_type_enum() ] };
	( @args $self:ident [ $( $stack:expr ),* ] ... ) => { &[ $($stack),* ] };
	( @args $self:ident [ $( $stack:expr ),* ] ) => { &[ $($stack),* ] };

	( @impl_fn_inner [ $( $stack:ident ),* ] $linkage:expr, $name:ident, ( $($args_tt:tt)* ), ( $($ret_tt:tt)* ), ( $( $inner_fn:ident )? ), $($tt:tt)* ) => {
		paste::paste! {
			#[doc(hidden)]
             // Prevent getting multiple warnings for each unused function.
			#[allow(dead_code)]
			fn [< add_f_ $name >](&mut self) {
				self.$name = Some(self.module.add_function(
					stringify!($name),
					__impl_functions_inner!(@ty self $($ret_tt)*).fn_type(__impl_functions_inner!(@args self [] $($args_tt)*), __impl_functions_inner!(@has_varargs $($args_tt)*)),
					$linkage,
				));

				$(
					let __current_block = self.builder.get_insert_block().unwrap();
					self.$inner_fn(self.$name.unwrap());
					if __current_block != self.builder.get_insert_block().unwrap() {
						self.builder.position_at_end(__current_block);
					}
				)?
			}

			pub fn [< f_ $name >](&mut self) -> FunctionValue<'ctx> {
				if self.$name.is_none() {
					self.[< add_f_ $name >]()
				}
				self.$name.unwrap()
			}
		}
	};

	( @impl_fn [ $( $stack:ident ),* ] @body $self:ident => $body:block, $linkage:expr, $name:ident, ( $($args_tt:tt)* ), ( $($ret_tt:tt)* ), $($tt:tt)* ) => {
		paste::paste! {
			impl<'ctx> Functions<'ctx> {
				#[doc(hidden)]
                // Prevent getting multiple warnings for each unused function.
                #[allow(dead_code)]
				#[inline(always)]
				fn [< add_f_ $name _inner >](&mut $self, $name: FunctionValue<'ctx>) -> () $body

				__impl_functions_inner!(@impl_fn_inner [$($stack,)* $name] $linkage, $name, ( $($args_tt)* ), ( $($ret_tt)* ), ( [< add_f_ $name _inner >] ), $($tt)*);
			}
		}

		__impl_functions_inner!([$($stack,)* $name] $($tt)*);
	};
	( @impl_fn [ $( $stack:ident ),* ] @no_body $linkage:expr, $name:ident, ( $($args_tt:tt)* ), ( $($ret_tt:tt)* ), $($tt:tt)* ) => {
		paste::paste! {
			impl<'ctx> Functions<'ctx> {
				__impl_functions_inner!(@impl_fn_inner [$($stack,)* $name] $linkage, $name, ( $($args_tt)* ), ( $($ret_tt)* ), (), $($tt)*);
			}
		}

		__impl_functions_inner!([$($stack,)* $name] $($tt)*);
	};

	( [ $( $stack:ident ),* ] ) => {
		#[derive(Clone, Debug)]
		pub struct Functions<'ctx> {
			ctx: &'ctx Context,
			builder: &'ctx Builder<'ctx>,
			module: Rc<Module<'ctx>>,

			$(
				$stack: Option<FunctionValue<'ctx>>,
			)*
		}

		impl<'ctx> Functions<'ctx> {
			pub fn new(
				ctx: &'ctx Context,
				builder: &'ctx Builder<'ctx>,
				module: Rc<Module<'ctx>>,
			) -> Self {
				Self {
					ctx,
					builder,
					module,

					$(
						$stack: None,
					)*
				}
			}
		}
	};

	( [ $( $stack:ident ),* ] fn $name:ident (&mut $self:ident, $arg_name:ident: FunctionValue<'ctx>) -> impl Fn($($args_tt:tt)*) -> ( $($ret_tt:tt)* ) $body:block $($tt:tt)*) => {
		__impl_functions_inner!(@impl_fn [$($stack),*] @body $self => $body, Some(Linkage::Private), $name, ( $($args_tt)* ), ( $($ret_tt)* ), $($tt)* );
	};
	( [ $( $stack:ident ),* ] fn $name:ident (&mut $self:ident, $arg_name:ident: FunctionValue<'ctx>) -> impl Fn($($args_tt:tt)*) $body:block $($tt:tt)*) => {
		__impl_functions_inner!(@impl_fn [$($stack),*] @body $self => $body, Some(Linkage::Private), $name, ( $($args_tt)* ), ( void ), $($tt)* );
	};

	( [ $( $stack:ident ),* ] extern "C" fn $name:ident ( $($args_tt:tt)* ) -> ( $($ret_tt:tt)* ) ; $($tt:tt)* ) => {
		__impl_functions_inner!(@impl_fn [$($stack),*] @no_body Some(Linkage::External), $name, ( $($args_tt)* ), ( $($ret_tt)* ), $($tt)* );
	};
	( [ $( $stack:ident ),* ] extern "C" fn $name:ident ( $($args_tt:tt)* ) ; $($tt:tt)* ) => {
		__impl_functions_inner!(@impl_fn [$($stack),*] @no_body Some(Linkage::External), $name, ( $($args_tt)* ), ( void ), $($tt)* );
	};
}

macro_rules! impl_functions {
	{ $($tt:tt)* } => { __impl_functions_inner!([] $($tt)*); };
}

impl_functions! {
	extern "C" fn getchar() -> (i32);
	extern "C" fn putchar(i32) -> (i32);
	extern "C" fn write(i32, *i8, i64) -> (i64);
}
