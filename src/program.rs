use crate::ast::{Ast, Node, NodeData, SpecialNodeData};
use crate::debug_info::DebugInfoBuilder;
use crate::error::Error;
use crate::functions::Functions;
use crate::{Args, OutputFormat};
use inkwell::builder::Builder;
use inkwell::context::Context;
use inkwell::module::{Linkage, Module};
use inkwell::passes::PassManager;
use inkwell::targets::{FileType, TargetMachine};
use inkwell::types::{BasicType, BasicTypeEnum};
use inkwell::values::{BasicValue, FunctionValue, IntValue, PointerValue};
use inkwell::{AddressSpace, IntPredicate};
use std::borrow::Cow;
use std::convert::TryInto;
use std::process::Command;
use std::rc::Rc;

pub struct Program<'ctx> {
	ctx: &'ctx Context,
	builder: &'ctx Builder<'ctx>,
	module: Rc<Module<'ctx>>,
	di: DebugInfoBuilder<'ctx>,

	fn_main: Option<FunctionValue<'ctx>>,
	args: Args,
	funcs: Functions<'ctx>,
	last_cell: Option<PointerValue<'ctx>>,
	last_value: Option<IntValue<'ctx>>,
}

impl<'ctx> Program<'ctx> {
	pub fn new(ctx: &'ctx Context, builder: &'ctx Builder<'ctx>, args: Args) -> Self {
		let module = Rc::new(ctx.create_module("bf-native"));
		let di = DebugInfoBuilder::new(&args.input, ctx, builder, &module);
		let funcs = Functions::new(ctx, builder, Rc::clone(&module));

		let mut program = Self {
			ctx,
			builder,
			module,
			di,
			fn_main: None,
			args,
			funcs,
			last_cell: None,
			last_value: None,
		};

		program.fn_main = Some(program.add_function("main", ctx.i32_type(), &[], false, true, 1));
		let entry = ctx.append_basic_block(program.fn_main.unwrap(), "entry");
		builder.position_at_end(entry);

		program
	}

	fn add_function(
		&mut self,
		name: &str,
		return_type: impl BasicType<'ctx>,
		parameter_types: &[BasicTypeEnum<'ctx>],
		has_varargs: bool,
		is_public: bool,
		line_num: u32,
	) -> FunctionValue<'ctx> {
		let linkage = if is_public {
			Some(Linkage::External)
		} else {
			Some(Linkage::Internal)
		};

		let fn_val = self.module.add_function(
			name,
			return_type.fn_type(parameter_types, has_varargs),
			linkage,
		);
		fn_val.set_subprogram(self.di.create_function_scope(
			name,
			&return_type,
			parameter_types,
			is_public,
			line_num,
		));

		fn_val
	}

	#[inline]
	fn end_function(&mut self, fn_val: FunctionValue<'ctx>) -> Result<(), Error> {
		self.di.end_function_scope(fn_val)
	}

	#[inline]
	fn invalidate_last_cell(&mut self) {
		self.last_cell = None;
		self.last_value = None;
	}

	/// Ensure that `self.last_cell` is `Some(_)`.
	fn ensure_last_cell(&mut self) {
		if self.last_cell.is_none() {
			let cells_ptr = self.builder.build_ptr_to_int(
				// self.globals.cells.as_pointer_value(),
				self.module.get_global("cells").unwrap().as_pointer_value(),
				self.ctx.i64_type(),
				"cells_ptr",
			);
			let ptr_value = self
				.builder
				.build_load(
					self.module.get_global("ptr").unwrap().as_pointer_value(),
					"ptr_value",
				)
				.as_basic_value_enum()
				.into_int_value();
			let cell_addr = self
				.builder
				.build_int_add(cells_ptr, ptr_value, "cell_addr");
			self.last_cell = Some(self.builder.build_int_to_ptr(
				cell_addr,
				self.ctx.i8_type().ptr_type(AddressSpace::Generic),
				"ptr",
			));
		}
	}

	fn build_load(&mut self) -> IntValue<'ctx> {
		self.ensure_last_cell();
		if let Some(last_value) = self.last_value {
			last_value
		} else {
			self.builder
				.build_load(self.last_cell.unwrap(), "value")
				.into_int_value()
		}
	}

	fn build_store(&mut self, value: IntValue<'ctx>) {
		self.ensure_last_cell();
		self.builder.build_store(self.last_cell.unwrap(), value);
		self.last_value = Some(value);
	}

	fn generate_node(&mut self, node: Node) {
		match node.data {
			NodeData::Prev => {
				let ptr = self.module.get_global("ptr").unwrap().as_pointer_value();
				let mut ptr_value = self.builder.build_load(ptr, "ptr_value").into_int_value();
				ptr_value = self.builder.build_int_sub(
					ptr_value,
					self.ctx.i64_type().const_int(1, false),
					"ptr_value",
				);
				self.builder.build_store(ptr, ptr_value);
				self.invalidate_last_cell();
			}
			NodeData::Next => {
				let ptr = self.module.get_global("ptr").unwrap().as_pointer_value();
				let mut ptr_value = self.builder.build_load(ptr, "ptr_value").into_int_value();
				ptr_value = self.builder.build_int_add(
					ptr_value,
					self.ctx.i64_type().const_int(1, false),
					"ptr_value",
				);
				self.builder.build_store(ptr, ptr_value);
				self.invalidate_last_cell();
			}
			NodeData::Dec => {
				let mut value = self.build_load();
				value =
					self.builder
						.build_int_sub(value, self.ctx.i8_type().const_int(1, false), "");
				self.build_store(value);
			}
			NodeData::Inc => {
				let mut value = self.build_load();
				value =
					self.builder
						.build_int_add(value, self.ctx.i8_type().const_int(1, false), "");
				self.build_store(value);
			}
			NodeData::Input => {
				let mut input = self
					.builder
					.build_call(self.funcs.f_getchar(), &[], "")
					.try_as_basic_value()
					.unwrap_left()
					.into_int_value();
				input = self
					.builder
					.build_int_cast(input, self.ctx.i8_type(), "input");
				self.build_store(input);
			}
			NodeData::Output => {
				let mut value = self.build_load();
				value = self.builder.build_int_cast(value, self.ctx.i32_type(), "");
				self.builder
					.build_call(self.funcs.f_putchar(), &[value.as_basic_value_enum()], "");
			}
			NodeData::Loop(inner) => {
				// Create 3 new blocks, place an unconditional branch to the first new block at the end of the current
				// block, then position the builder at the end of the first new block.
				let current_block = self.builder.get_insert_block().unwrap();
				let loop_entry_block = self
					.ctx
					.insert_basic_block_after(current_block, "loop_entry");
				let loop_body_block = self
					.ctx
					.insert_basic_block_after(loop_entry_block, "loop_body");
				let loop_end_block = self
					.ctx
					.insert_basic_block_after(loop_body_block, "loop_end");
				self.builder.position_at_end(current_block);
				self.builder.build_unconditional_branch(loop_entry_block);
				self.builder.position_at_end(loop_entry_block);
				self.invalidate_last_cell();

				let cell = self.build_load();
				let zero = self.ctx.i8_type().const_zero();
				let is_cell_zero =
					self.builder
						.build_int_compare(IntPredicate::EQ, cell, zero, "is_cell_zero");
				self.builder.build_conditional_branch(
					is_cell_zero,
					loop_end_block,
					loop_body_block,
				);

				self.builder.position_at_end(loop_body_block);
				self.invalidate_last_cell();
				for node in inner {
					self.generate_node(node);
				}
				self.builder.build_unconditional_branch(loop_entry_block);

				self.builder.position_at_end(loop_end_block);
				self.invalidate_last_cell();
			}
			NodeData::Special(special) => match special {
				SpecialNodeData::CachedOutput => {
					let value_cast = self.builder.build_int_cast(
						self.last_value
							.expect("cached output node does not have last value"),
						self.ctx.i32_type(),
						"",
					);
					self.builder.build_call(
						self.funcs.f_putchar(),
						&[value_cast.as_basic_value_enum()],
						"",
					);
				}
			},
		}
	}

	pub fn generate(&mut self) -> Result<(), Error> {
		let mut ast = Ast::new(
			&self.args.input,
			self.args.cell_count,
			self.args.const_iters,
		)
		.map_err(|e| Error(format!("Failed to generate AST: {}", e)))?;
		// TODO: fix optimisations with debug info
		// ast.optimise();
		let Ast {
			ast,
			cells,
			output,
			ptr,
			..
		} = ast;
		let ptr = ptr.try_into().expect("ptr > u64::MAX");

		if !ast.is_empty() {
			let cells_len = cells.len();

			let cell_count = (ast.len() > 0)
				.then(|| {
					self.args
						.cell_count
						.try_into()
						.expect("cell_count > usize::MAX")
				})
				.unwrap_or(cells_len);

			let g_cells = self.module.add_global(
				self.ctx
					.i8_type()
					.array_type(cell_count.try_into().expect("cell_count > u32::MAX")),
				Some(AddressSpace::Local),
				"cells",
			);
			g_cells.set_initializer(
				&self.ctx.i8_type().const_array(
					&cells
						.into_iter()
						.chain((cells_len..cell_count).map(|_| 0))
						.map(|cell| self.ctx.i8_type().const_int(cell as u64, false))
						.collect::<Vec<_>>(),
				),
			);

			let g_ptr =
				self.module
					.add_global(self.ctx.i64_type(), Some(AddressSpace::Local), "ptr");
			g_ptr.set_initializer(&self.ctx.i64_type().const_int(ptr, false));
		}

		if !output.is_empty() {
			let count = output.len();

			let precomputed_output = self.module.add_global(
				self.ctx.i8_type().array_type(
					count
						.try_into()
						.expect("failed to convert output length to u32"),
				),
				Some(AddressSpace::Const),
				"precomputed_output",
			);
			precomputed_output.set_initializer(
				&self.ctx.i8_type().const_array(
					&output
						.into_iter()
						.map(|b| self.ctx.i8_type().const_int(b as u64, false))
						.collect::<Vec<_>>(),
				),
			);

			self.builder.build_call(
				self.funcs.f_write(),
				&[
					// TODO: `stdout` may not always be 1 for a given platform - how can we check this dynamically?
					self.ctx
						.i32_type()
						.const_int(1, false)
						.as_basic_value_enum(),
					precomputed_output
						.as_pointer_value()
						.const_cast(self.ctx.i8_type().ptr_type(AddressSpace::Generic))
						.as_basic_value_enum(),
					self.ctx
						.i64_type()
						.const_int(count as u64, false)
						.as_basic_value_enum(),
				],
				"",
			);
		}

		for node in ast {
			self.generate_node(node);
		}

		self.builder
			.build_return(Some(&self.ctx.i32_type().const_int(0, false)));
		self.end_function(self.fn_main.unwrap())?;

		self.di.finalise();
		self.module
			.verify()
			.map_err(|e| Error(format!("Failed to verify module:\n{}", e)))?;
		Ok(())
	}

	pub fn emit(
		&self,
		pass_manager: &PassManager<Module<'ctx>>,
		target_machine: TargetMachine,
	) -> Result<(), String> {
		let module = self.module.clone();
		module.set_triple(&target_machine.get_triple());
		module.set_data_layout(&target_machine.get_target_data().get_data_layout());
		pass_manager.run_on(&module);

		match self.args.emit {
			OutputFormat::LlvmIr => match self.args.output.as_ref() {
				Some(output) => module.print_to_file(&output).unwrap(),
				None => println!("{}", module.print_to_string().to_string()),
			},
			OutputFormat::Object => match self.args.output.as_ref() {
				Some(output) => target_machine
					.write_to_file(&module, FileType::Object, output)
					.unwrap(),
				None => panic!("cannot emit an object file to stdout"),
			},
			OutputFormat::Binary => match self.args.output.as_ref() {
				Some(output) => {
					let obj_path = std::env::temp_dir().join("bf-native.o");
					target_machine
						.write_to_file(&module, FileType::Object, &obj_path)
						.unwrap();

					let linker_status = Command::new("clang")
						.args(&[
							"-target",
							&target_machine.get_triple().as_str().to_string_lossy(),
							"-o",
							output.as_os_str().to_string_lossy().as_ref(),
							obj_path.as_os_str().to_string_lossy().as_ref(),
						])
						.spawn()
						.map_err(|e| format!("linker did not execute successfully: {}", e))?
						.wait()
						.map_err(|e| e.to_string())?;

					if !linker_status.success() {
						return Err(format!(
							"Linker did not execute successfully: {}",
							linker_status
								.code()
								.map_or(Cow::Borrowed("exited with signal"), |code| Cow::Owned(
									format!("exited with status code {}", code)
								))
								.as_ref()
						));
					}
				}
				None => panic!("cannot emit an binary to stdout"),
			},
		}

		Ok(())
	}
}
