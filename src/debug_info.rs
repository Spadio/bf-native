use crate::ast::Location;
use crate::error::Error;
use inkwell::builder::Builder;
use inkwell::context::Context;
use inkwell::debug_info::{
	self, AsDIScope, DIFlags, DIFlagsConstants, DIScope, DISubprogram, DIType, DWARFEmissionKind,
	DWARFSourceLanguage,
};
use inkwell::module::Module;
use inkwell::types::{AnyType, BasicTypeEnum};
use inkwell::values::FunctionValue;
use std::collections::HashMap;
use std::convert::TryInto;
use std::path::Path;

pub struct DebugInfoBuilder<'ctx> {
	ctx: &'ctx Context,
	builder: &'ctx Builder<'ctx>,
	di_builder: debug_info::DebugInfoBuilder<'ctx>,
	compile_unit: debug_info::DICompileUnit<'ctx>,

	type_map: HashMap<String, DIType<'ctx>>,
	lexical_blocks: Vec<DIScope<'ctx>>,
}

impl<'ctx> DebugInfoBuilder<'ctx> {
	pub fn new(
		source_path: &Path,
		ctx: &'ctx Context,
		builder: &'ctx Builder<'ctx>,
		module: &Module<'ctx>,
	) -> Self {
		let (di_builder, compile_unit) = module.create_debug_info_builder(
			true,
			DWARFSourceLanguage::C,
			&source_path.file_name().unwrap().to_string_lossy(),
			&source_path.parent().unwrap().to_string_lossy(),
			"bf-native",
			false,
			"",
			0,
			"",
			DWARFEmissionKind::Full,
			0,
			true,
			false,
			"",
			"",
		);

		let mut di_builder = Self {
			ctx,
			builder,
			di_builder,
			compile_unit,

			lexical_blocks: Vec::new(),
			type_map: HashMap::new(),
		};
		di_builder.init_type_map();
		di_builder
	}

	/// Insert all of the types we are going to use into the type map.
	fn init_type_map(&mut self) {
		self.create_type("i8", &self.ctx.i8_type(), 8, true);
		self.create_type("i16", &self.ctx.i16_type(), 16, true);
		self.create_type("i32", &self.ctx.i32_type(), 32, true);
		self.create_type("i64", &self.ctx.i64_type(), 64, true);
		self.create_type("u8", &self.ctx.i8_type(), 8, true);
		self.create_type("u16", &self.ctx.i16_type(), 16, true);
		self.create_type("u32", &self.ctx.i32_type(), 32, true);
		self.create_type("u64", &self.ctx.i64_type(), 64, true);
	}

	fn create_type(
		&mut self,
		name: &str,
		ty: &dyn AnyType<'ctx>,
		size_in_bits: u64,
		is_public: bool,
	) {
		let flags = if is_public {
			DIFlags::PUBLIC
		} else {
			DIFlags::PRIVATE
		};
		self.type_map.insert(
			ty.print_to_string().to_string(),
			self.di_builder
				.create_basic_type(name, size_in_bits, 0x00, flags)
				.expect("could not create basic debug info type")
				.as_type(),
		);
	}

	#[inline]
	fn get_di_type_from_name(&self, type_name: &str) -> DIType<'ctx> {
		*self
			.type_map
			.get(type_name)
			.unwrap_or_else(|| panic!("type does not exist in type map: {}", type_name))
	}

	pub fn create_function_scope(
		&mut self,
		name: &str,
		return_type: &dyn AnyType<'ctx>,
		parameter_types: &[BasicTypeEnum<'ctx>],
		is_public: bool,
		line_num: u32,
	) -> DISubprogram<'ctx> {
		let flags = if is_public {
			DIFlags::PUBLIC
		} else {
			DIFlags::PRIVATE
		};
		let return_type = match return_type.print_to_string().to_string() {
			return_type if return_type == "void" => None,
			return_type => Some(self.get_di_type_from_name(&return_type)),
		};
		let parameter_types = parameter_types
			.into_iter()
			.map(|parameter_type| self.get_di_type_from_name(&basic_type_name(parameter_type)))
			.collect::<Vec<_>>();

		let subroutine_type = self.di_builder.create_subroutine_type(
			self.compile_unit.get_file(),
			return_type,
			&parameter_types,
			flags,
		);

		let subprogram = self.di_builder.create_function(
			self.compile_unit.as_debug_info_scope(),
			name,
			None,
			self.compile_unit.get_file(),
			line_num,
			subroutine_type,
			!is_public,
			true,
			line_num,
			flags,
			false,
		);
		self.lexical_blocks.push(subprogram.as_debug_info_scope());
		subprogram
	}

	pub fn end_function_scope(&mut self, fn_val: FunctionValue<'ctx>) -> Result<(), Error> {
		let block = self.lexical_blocks.pop().ok_or_else(|| {
			Error(format!(
				"Attempted to end function \"{}\", the stack of lexical blocks was empty",
				fn_val.get_name().to_string_lossy(),
			))
		})?;
		let scope = fn_val
			.get_subprogram()
			.ok_or_else(|| {
				Error(format!(
					"Function \"{}\" was not registered using Program::add_function",
					fn_val.get_name().to_string_lossy()
				))
			})?
			.as_debug_info_scope();

		if block == scope {
			Ok(())
		} else {
			Err(Error(format!(
				"Attempted to end function \"{}\", but it was not started",
				fn_val.get_name().to_string_lossy()
			)))
		}
	}

	pub fn set_location(&self, location: Location) {
		let scope = if let Some(lexical_block) = self.lexical_blocks.last() {
			*lexical_block
		} else {
			self.compile_unit.as_debug_info_scope()
		};

		let (line, col) = location.line_col();
		self.builder.set_current_debug_location(
			self.ctx,
			self.di_builder.create_debug_location(
				self.ctx,
				line.try_into().expect("line > u32::MAX"),
				col.try_into().expect("col > u32::MAX"),
				scope,
				None,
			),
		)
	}

	pub fn finalise(&self) {
		self.di_builder.finalize();
	}
}

fn basic_type_name(basic_type: &BasicTypeEnum) -> String {
	match basic_type {
		BasicTypeEnum::ArrayType(ty) => ty.print_to_string().to_string(),
		BasicTypeEnum::IntType(ty) => ty.print_to_string().to_string(),
		BasicTypeEnum::FloatType(ty) => ty.print_to_string().to_string(),
		BasicTypeEnum::PointerType(ty) => ty.print_to_string().to_string(),
		BasicTypeEnum::StructType(ty) => ty.print_to_string().to_string(),
		BasicTypeEnum::VectorType(ty) => ty.print_to_string().to_string(),
	}
}
