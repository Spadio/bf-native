#!/usr/bin/env bash

RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[1;33m"
CLEAR="\033[0m"

for PROG in "$(dirname "$0")/progs/"*.b
do
    echo -ne "${YELLOW}Compiling '$(basename "$PROG")'... ${CLEAR}"

    STDERR=$((cargo run -q -- -o "${PROG%.*}" "$PROG") 2>&1 >/dev/null)
    STATUS=$?
    if [[ $STATUS -eq 0 ]]; then
        echo -e "${GREEN}SUCCESS${CLEAR}"
    else
        echo -e "${RED}FAILURE (exited with status code: ${STATUS})\nstderr:${CLEAR}\n${STDERR}"
    fi
done
